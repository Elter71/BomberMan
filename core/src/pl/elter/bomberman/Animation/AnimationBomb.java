package pl.elter.bomberman.Animation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationBomb extends AnimationGroup {

	private Animation bombAnimation;

	public AnimationBomb(Texture AnimationTexture, int TextureSize, int TextureStartY, int TextureStartX) {
		super(AnimationTexture, TextureSize, TextureStartY, TextureStartX);
		
		createAnimationFrame();
	}

	@Override
	protected void createAnimationFrame() {
		TextureRegion[] animRegion = new TextureRegion[3];

		for (int i = 0; i < animRegion.length; i++) {
			animRegion[i] = new TextureRegion(AnimationTexture, TextureStartX * TextureSize,
					TextureStartY * (i + TextureSize), TextureSize, TextureSize);
		}
		bombAnimation = new Animation(1f, animRegion);

	}

	public Animation getBombAnimation() {
		return bombAnimation;
	}

}
