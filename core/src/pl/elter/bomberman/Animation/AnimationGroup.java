package pl.elter.bomberman.Animation;

import com.badlogic.gdx.graphics.Texture;

abstract public class AnimationGroup {
	
	protected Texture AnimationTexture;
	protected int TextureSize;
	protected int TextureStartY;
	protected int TextureStartX;
	
	
	public AnimationGroup(Texture AnimationTexture, int TextureSize, int TextureStartY,int TextureStartX) {
		
		this.AnimationTexture = AnimationTexture;
		this.TextureSize = TextureSize;
		this.TextureStartX = TextureStartX;
		this.TextureStartY = TextureStartY;
		
	}
	
	abstract protected void createAnimationFrame();

}
