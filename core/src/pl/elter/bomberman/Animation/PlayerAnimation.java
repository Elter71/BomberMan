package pl.elter.bomberman.Animation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PlayerAnimation {

	private Animation WalkUpAnimation;
	private Animation WalkDownAnimation;
	private Animation WalkLeftAnimation;
	private Animation WalkRightAnimation;

	private Texture AnimationTexture;
	private int TextureSize;
	private int PlayerStartY; // wiersz w ktorym zaczyna się postać
								// (to * TextureSize)

	public PlayerAnimation(Texture AnimationTexture, int TextureSize, int PlayerStartY) {
		this.AnimationTexture = AnimationTexture;
		this.TextureSize = TextureSize;
		this.PlayerStartY = PlayerStartY;

		createAnimationFrame();

	}

	private void createAnimationFrame() {
		
		TextureRegion[] animRegion = new TextureRegion[3];

		for (int i = 0; i < animRegion.length; i++) {
			animRegion[i] = new TextureRegion(AnimationTexture, (i + 1) * TextureSize, PlayerStartY * TextureSize,
					TextureSize, TextureSize);
		}

		WalkDownAnimation = new Animation(0.22f, animRegion);

		animRegion = new TextureRegion[3];

		animRegion[0] = new TextureRegion(AnimationTexture, (0) * TextureSize, PlayerStartY * TextureSize, TextureSize,
				TextureSize);

		for (int i = 1; i < animRegion.length; i++) {
			animRegion[i] = new TextureRegion(AnimationTexture, (i + 7) * TextureSize, PlayerStartY * TextureSize,
					TextureSize, TextureSize);
		}
		WalkUpAnimation = new Animation(0.22f, animRegion);

		animRegion = new TextureRegion[4];

		for (int i = 0; i < animRegion.length; i++) {
			animRegion[i] = new TextureRegion(AnimationTexture, (i + 4) * TextureSize, PlayerStartY * TextureSize,
					TextureSize, TextureSize);
		}
		WalkRightAnimation = new Animation(0.22f, animRegion);
		
		animRegion = new TextureRegion[4];

		for (int i = 0; i < animRegion.length; i++) {
			animRegion[i] = new TextureRegion(AnimationTexture, (i + 4) * TextureSize, PlayerStartY * TextureSize,
					TextureSize, TextureSize);
			animRegion[i].flip(true, false);
		}
		WalkLeftAnimation = new Animation(0.22f, animRegion);

	}

	public Animation getWalkUpAnimation() {
		return WalkUpAnimation;
	}

	public Animation getWalkDownAnimation() {
		return WalkDownAnimation;
	}

	public Animation getWalkLeftAnimation() {
		return WalkLeftAnimation;
	}

	public Animation getWalkRightAnimation() {
		return WalkRightAnimation;
	}

}
