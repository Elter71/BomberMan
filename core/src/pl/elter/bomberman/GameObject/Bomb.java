package pl.elter.bomberman.GameObject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import pl.elter.bomberman.Animation.AnimationBomb;
import pl.elter.bomberman.GeneralSettings.Settings;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;

public class Bomb extends GameObject {

	private AnimationBomb animationBomba;

	private float x, y;

	public boolean isCreate;

	private float TimeDelta;

	private float LifeTime = 3.0f;

	public boolean isOverlaps;

	private boolean isFixet = false;

	public Bomb(Texture texture, String name, World world, float x, float y) {
		super(texture, name, world);

		this.x = x;
		this.y = y;

		animationBomba = new AnimationBomb(texture, 16, 15, 11);

		sprite.setRegion(animationBomba.getBombAnimation().getKeyFrames()[0]);

		createBox2D();
		spitePositionFromBody();

		isCreate = false;
		isOverlaps = true;

	}

	protected void spitePositionFromBody() {
		sprite.setPosition(ObjectBody.getPosition().x - 8.5f, ObjectBody.getPosition().y - 7f);
	}

	@Override
	public void Draw(SpriteBatch batch) {
		sprite.draw(batch);

	}

	private void createBox2D() {

		define2DPhysics();

		this.ObjectBodyDef.position.set(x, y);
		this.ObjectBodyDef.type = BodyType.KinematicBody;

		this.ObjectBody = world.createBody(ObjectBodyDef);

		this.ObjectBody.setUserData(this.getName());

		CircleShape BoxShape = new CircleShape();
		BoxShape.setRadius(5f);

		this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_BOMB;

<<<<<<< HEAD
		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_GROUND;
=======
		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_GROUND | Settings.BIT_PLAYER | Settings.BIT_BOX;
>>>>>>> Box

		this.ObjectFixtureDef.shape = BoxShape;
		this.ObjectBody.createFixture(ObjectFixtureDef);

	}

	public void act(float delta) {
		if (isCreate) {
			TimeDelta += delta;
			sprite.setRegion(animationBomba.getBombAnimation().getKeyFrame(TimeDelta, true));

			if (TimeDelta > LifeTime) {
				TimeDelta = 0;
<<<<<<< HEAD
				Boom();
=======
				isErupts = true;
				setActive(true);
			}
			if (isErupts && TimeDelta > 0.6f) {
				TimeDelta = 0;
				checkWhereErups();
				createEruptsBox();
				isboom = true;
>>>>>>> Box

			}
			// if (!isOverlaps)
			// Fixturechange((short) 6);
		}
	}

	public void setPos(float x, float y) {

<<<<<<< refs/remotes/origin/master
		// Fixturechange(Settings.BIT_GROUND);
		this.x = (x - 8) / Settings.Texture_size;
		this.y = (y - 8) / Settings.Texture_size;

		System.out.println("bomb pos: " + this.x + "||" + this.y);
=======
		//Fixturechange(Settings.BIT_GROUND);
>>>>>>> Revert "Merged Bomb into master"
		this.ObjectBody.setTransform(new Vector2(x, y), this.ObjectBody.getAngle());
		spitePositionFromBody();
		isCreate = true;
		isOverlaps = true;

	}

	public void Fixturechange(short BIT_SETING) { // zmiana aby bomba byla podatna na kolizje

		//if (!isFixet) {

			System.out.println("fix");

			CircleShape BoxShape = new CircleShape();
			BoxShape.setRadius(5f);

			this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_BOMB;

			this.ObjectFixtureDef.filter.maskBits = BIT_SETING;

			this.ObjectFixtureDef.shape = BoxShape;

<<<<<<< HEAD
			this.ObjectBody.createFixture(ObjectFixtureDef);
			isFixet = true;
		//}
=======
		this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_BOMB;

		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_GROUND | Settings.BIT_PLAYER | Settings.BIT_BOX;
>>>>>>> Box

	}

	private void Boom() {
		System.out.println(isOverlaps);
		System.out.println(this.ObjectBody.getFixtureList().get(0).getFilterData().maskBits);
		this.ObjectBody.setTransform(new Vector2(500, 500), this.ObjectBody.getAngle());
		spitePositionFromBody();
		isCreate = false;
<<<<<<< refs/remotes/origin/master
		isErupts = false;
		isboom = false;
		setActive(false);
		if(this.ObjectBody.getFixtureList().size>2){
			this.ObjectBody.destroyFixture(this.ObjectBody.getFixtureList().get(2));
			this.ObjectBody.destroyFixture(this.ObjectBody.getFixtureList().get(1));
		}

		for(int i=0; i<whereErupts.length;i++)
			whereErupts[i] = false;
		
=======
		isOverlaps = false;
		isFixet = false;
		// Fixturechange((short) -1);

		System.out.println(this.ObjectBody.getFixtureList().get(0).getFilterData().maskBits);
>>>>>>> Revert "Merged Bomb into master"
		System.out.println("BOOM!!!!");

	}

<<<<<<< refs/remotes/origin/master
	/**
	 * @return the wallMap
	 */
	public int[][] getWallMap() {
		return wallMap;
	}

	/**
	 * @param wallMap
	 *            the wallMap to set
	 */
	public void setWallMap(int wallMap[][]) {
		this.wallMap = wallMap;
	}

	public void reload() {
		Boom();
		
	}

=======
>>>>>>> Revert "Merged Bomb into master"
}
