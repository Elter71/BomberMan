package pl.elter.bomberman.GameObject;

import java.util.Set;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import pl.elter.bomberman.GeneralSettings.Settings;

public class Box extends GameObject {

	private int PosTextureY = 13;
	private int PosTextureX = 9;

	private float PosX;
	private float PosY;

	public Box(Texture texture, String name, World world, float PosX, float PosY) {
		super(texture, name, world);

		this.sprite = new Sprite(new TextureRegion(this.sprite.getTexture(), PosTextureX * Settings.Texture_size,
				PosTextureY * Settings.Texture_size, Settings.Texture_size, Settings.Texture_size));
		this.PosX = PosX+0.5f;
		this.PosY = PosY+0.5f;

		createBox2D();

		this.spitePositionFromBody();

		// -> 9 | 15 -- 9/15

	}

	@Override
	public void Draw(SpriteBatch batch) {
		this.sprite.draw(batch);
	}

	private void createBox2D() {
		define2DPhysics();

		this.ObjectBodyDef.position.set((PosX * Settings.Texture_size), (PosY * Settings.Texture_size));
		this.ObjectBodyDef.type = BodyType.DynamicBody;

		this.ObjectBody = world.createBody(ObjectBodyDef);

		PolygonShape BoxSape = new PolygonShape();
		BoxSape.setAsBox(8f, 8f);

		this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_BOX;

		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_PLAYER | Settings.BIT_BOMB;

		this.ObjectFixtureDef.shape = BoxSape;
		this.ObjectBody.createFixture(ObjectFixtureDef);
		
		MassData mass = new MassData();
		mass.mass = 1000000;
		this.ObjectBody.setMassData(mass);

	}

	public void Destroy() {
		//this.ObjectBody.destroyFixture(this.ObjectBody.getFixtureList().get(0));
		this.ObjectBody.applyForceToCenter(5000,500,true);
		this.sprite.setPosition(700, 500);
		this.ObjectBody.setTransform(700, 500, 0);
		
	}
	public void reload(){
		this.ObjectBody.setTransform((PosX * Settings.Texture_size), (PosY * Settings.Texture_size), 0);
		this.spitePositionFromBody();
	}

}
