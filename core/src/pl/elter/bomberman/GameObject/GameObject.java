package pl.elter.bomberman.GameObject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class GameObject{

	private String name;

	protected Sprite sprite;
	protected Body ObjectBody;
	protected BodyDef ObjectBodyDef;
	protected FixtureDef ObjectFixtureDef;

	protected World world;

	public GameObject(Texture texture, String name, World world) {

		if (texture != null) {
			sprite = new Sprite(texture);
			sprite.setSize(16, 16);
		}
		if (name != null)
			this.name = name;
		if (world != null)
			this.world = world;

	}

	abstract public void Draw(SpriteBatch batch);

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	protected void define2DPhysics() {
		ObjectBodyDef = new BodyDef();
		ObjectFixtureDef = new FixtureDef();

	}

	protected void spitePositionFromBody() {
		sprite.setPosition(ObjectBody.getPosition().x - 8.5f, ObjectBody.getPosition().y - 8.5f);

	}


	public Body getBody() {
		return this.ObjectBody;
	}

}
