package pl.elter.bomberman.GameObject;

<<<<<<< HEAD
import java.util.Set;
=======
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LoggingMXBean;
>>>>>>> Box

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import pl.elter.bomberman.GeneralSettings.Settings;

import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Map extends GameObject {

	private TiledMap map;
	private OrthogonalTiledMapRenderer mapRender;

	private float textureSize;

<<<<<<< HEAD
=======
	public int Map[][];
	
	public List<Box> boxlist;

>>>>>>> Box
	public Map(String name, World world) {
		super(null, name, world);

		map = new TmxMapLoader().load("tmx/mapa.tmx");
		mapRender = new OrthogonalTiledMapRenderer(map);
		boxlist = new ArrayList<Box>();

		createBoxFromMap();
		createBox();

	}

	@Override
	public void Draw(SpriteBatch batch) {
		// TODO Auto-generated method stub

	}

	public void Draw(SpriteBatch batch, OrthographicCamera camera) {
		mapRender.setView(camera);
		mapRender.render();
		batch.begin();
		if(boxlist.size() >0){
			for(int i=0; i<boxlist.size();i++)
				boxlist.get(i).Draw(batch);
			
		}
		batch.end();
			
	}

	private void createBoxFromMap() {

		define2DPhysics();

		// Tworzenie box2d dla ścian zewnętrznych
		TiledMapTileLayer mapLayer = (TiledMapTileLayer) map.getLayers().get("Sciany");

		textureSize = mapLayer.getTileHeight();

		for (int y = mapLayer.getHeight(); y >= 0; y--) {
			for (int x = 0; x < mapLayer.getWidth(); x++) {

				Cell cell = mapLayer.getCell(x, y);

				if (cell == null)
					continue;

				this.ObjectBodyDef.position.x = (x + 0.5f) * textureSize;
				this.ObjectBodyDef.position.y = (y + 0.5f) * textureSize;
				this.ObjectBodyDef.type = BodyType.StaticBody;

				this.ObjectBody = world.createBody(ObjectBodyDef);
				this.ObjectBody.setUserData(this.getName());

				// kiedy moze jako ChainShape;
				PolygonShape boxShape = new PolygonShape();
				boxShape.setAsBox(textureSize / 2, textureSize / 2);
				
				this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_GROUND;

				this.ObjectFixtureDef.filter.maskBits = Settings.BIT_PLAYER | Settings.BIT_BOMB;

				this.ObjectFixtureDef.shape = boxShape;
				this.ObjectBody.createFixture(ObjectFixtureDef);

			}

		}
		// Tworzenie box2d dla ścian wewnętrznych
		mapLayer = (TiledMapTileLayer) map.getLayers().get("Sciany_srodek");

		textureSize = mapLayer.getTileHeight();

		for (int y = mapLayer.getHeight(); y >= 0; y--) {
			for (int x = 0; x < mapLayer.getWidth(); x++) {

				Cell cell = mapLayer.getCell(x, y);

				if (cell == null)
					continue;

				this.ObjectBodyDef.position.x = (x + 0.5f) * textureSize;
				this.ObjectBodyDef.position.y = (y + 0.5f) * textureSize;
				this.ObjectBodyDef.type = BodyType.StaticBody;

				this.ObjectBody = world.createBody(ObjectBodyDef);
				this.ObjectBody.setUserData(this.getName());

				// kiedy moze jako ChainShape;
				PolygonShape boxShape = new PolygonShape();
				boxShape.setAsBox(textureSize / 2, textureSize / 2);

				this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_GROUND;

				this.ObjectFixtureDef.filter.maskBits = Settings.BIT_PLAYER | Settings.BIT_BOMB;

				this.ObjectFixtureDef.shape = boxShape;
				this.ObjectBody.createFixture(ObjectFixtureDef);

			}

		}

<<<<<<< HEAD
=======

	}
	
	private void createBox(){
		for (int y = 14; y >= 0; y--) {
			for (int x = 0; x < 15; x++) {
				if(Map[x][y]==0)
					if(x==1 && y>2 && y<12 || x==2 && y>1 && y<13 || x>2 && x<12 || 
							x==12 && y>2 && y<12 || x==13 && y>2 && y<12)
					boxlist.add(new Box(new Texture("bomb_party_v4.png"),"Box"+y+x,this.world,x,y));
			}
			System.out.println();
		}
>>>>>>> Box
	}

	public void reload() {
		for(int i=0;i<boxlist.size();i++)
			boxlist.get(i).reload();
		
	}
}
