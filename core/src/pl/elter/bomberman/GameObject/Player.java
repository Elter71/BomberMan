package pl.elter.bomberman.GameObject;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import pl.elter.bomberman.Animation.PlayerAnimation;
import pl.elter.bomberman.GeneralSettings.Settings;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Player extends GameObject {

	private float moveSpeed = 1.5f;
	private boolean isMove = false;

	private Vector2 Przemieszczenie;

	private PlayerAnimation playerAnimation;

	private float TimeDelta;

	private KierunekRuchu kierunekRuchu;
	
	private Bomb overpalsBomb;

	private boolean isPlayerOverpalsBomb;

	public Player(Texture tex, String name, World world) {
		super(tex, name, world);
		createBox2D();
		playerAnimation = new PlayerAnimation(tex, 16, 17);
		spitePositionFromBody();
<<<<<<< HEAD
		
=======

		isPlayerOverpalsBomb = true;
>>>>>>> Box
	}

	@Override
	public void Draw(SpriteBatch batch) {
		sprite.draw(batch);

	}

	public void act(float delta) {
		TimeDelta += delta;

		Przemieszczenie = this.ObjectBody.getLinearVelocity();
		
		if (overpalsBomb != null) {
			if (this.ObjectBody.getPosition().y - overpalsBomb.ObjectBody.getPosition().y > 2.5f)
				isPlayerOverpalsBomb = false;
			if (this.ObjectBody.getPosition().y - overpalsBomb.ObjectBody.getPosition().y < -2.5f)
				isPlayerOverpalsBomb = false;
			if (this.ObjectBody.getPosition().x - overpalsBomb.ObjectBody.getPosition().x > 2.5f)
				isPlayerOverpalsBomb = false;
			if (this.ObjectBody.getPosition().x - overpalsBomb.ObjectBody.getPosition().x < -2.5f)
				isPlayerOverpalsBomb = false;
		}
		
		if (overpalsBomb != null && overpalsBomb.isOverlaps && !isPlayerOverpalsBomb)
			overpalsBomb.setActive(true);

		if (isMove == false) {
			stopPlayer();

			if (kierunekRuchu == null)
				sprite.setRegion(playerAnimation.getWalkDownAnimation().getKeyFrames()[0]);
			if (kierunekRuchu == KierunekRuchu.Dol)
				sprite.setRegion(playerAnimation.getWalkDownAnimation().getKeyFrames()[0]);
			if (kierunekRuchu == KierunekRuchu.Gora)
				sprite.setRegion(playerAnimation.getWalkUpAnimation().getKeyFrames()[0]);
			if (kierunekRuchu == KierunekRuchu.Prawo)
				sprite.setRegion(playerAnimation.getWalkRightAnimation().getKeyFrames()[0]);
			if (kierunekRuchu == KierunekRuchu.Lewo)
				sprite.setRegion(playerAnimation.getWalkLeftAnimation().getKeyFrames()[0]);
		}

		if (isMove == true) {

			if (kierunekRuchu == KierunekRuchu.Dol)
				sprite.setRegion(playerAnimation.getWalkDownAnimation().getKeyFrame(TimeDelta, true));
			if (kierunekRuchu == KierunekRuchu.Gora)
				sprite.setRegion(playerAnimation.getWalkUpAnimation().getKeyFrame(TimeDelta, true));
			if (kierunekRuchu == KierunekRuchu.Prawo)
				sprite.setRegion(playerAnimation.getWalkRightAnimation().getKeyFrame(TimeDelta, true));
			if (kierunekRuchu == KierunekRuchu.Lewo)
				sprite.setRegion(playerAnimation.getWalkLeftAnimation().getKeyFrame(TimeDelta, true));
		}

		spitePositionFromBody();
	}

	private void createBox2D() {
		define2DPhysics();

		this.ObjectBodyDef.position.set(16, 16);
		this.ObjectBodyDef.type = BodyType.DynamicBody;

		this.ObjectBody = world.createBody(ObjectBodyDef);

		
		
		PolygonShape BoxShape = new PolygonShape();
		BoxShape.setAsBox(6f, 6f);
		
		this.ObjectFixtureDef.filter.categoryBits = Settings.BIT_PLAYER;
<<<<<<< HEAD
		
		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_GROUND | Settings.BIT_BOMB;
=======

		this.ObjectFixtureDef.filter.maskBits = Settings.BIT_GROUND | Settings.BIT_BOMB | Settings.BIT_BOX;
>>>>>>> Box

		this.ObjectFixtureDef.shape = BoxShape;
		this.ObjectBody.createFixture(ObjectFixtureDef);
	}

	public void movePlayer(KierunekRuchu kierunek) {

		kierunekRuchu = kierunek;

		if (KierunekRuchu.Gora == kierunek && Przemieszczenie.y < 25f) {
			this.ObjectBody.applyLinearImpulse(0, 30f * moveSpeed, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);
			setMove(true);
<<<<<<< HEAD
			//if(overpalsBomb != null)
				//overpalsBomb.isOverlaps = false;
=======
			
>>>>>>> Box
		}

		if (KierunekRuchu.Dol == kierunek && Przemieszczenie.y > -25f) {
			this.ObjectBody.applyLinearImpulse(0, -30f * moveSpeed, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);
			setMove(true);
<<<<<<< HEAD
			/*if(overpalsBomb != null)
			overpalsBomb.isOverlaps = false;*/
=======
>>>>>>> Box
		}

		if (KierunekRuchu.Lewo == kierunek && Przemieszczenie.x > -25f) {
			this.ObjectBody.applyLinearImpulse(-25f * moveSpeed, 0, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);
			setMove(true);
<<<<<<< HEAD
			/*if(overpalsBomb != null)
			overpalsBomb.isOverlaps = false;*/
=======
>>>>>>> Box
		}

		if (KierunekRuchu.Prawo == kierunek && Przemieszczenie.x < 25f) {
			this.ObjectBody.applyLinearImpulse(25f * moveSpeed, 0, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);
			setMove(true);
<<<<<<< HEAD
			/*if(overpalsBomb != null)
			overpalsBomb.isOverlaps = false;*/
=======
>>>>>>> Box
		}
	}

	private void stopPlayer() {
		if (Przemieszczenie.y > 25f)
			this.ObjectBody.applyLinearImpulse(0, -30f * moveSpeed, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);

		if (Przemieszczenie.y < -25f)
			this.ObjectBody.applyLinearImpulse(0, 30f * moveSpeed, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);

		if (Przemieszczenie.x > 25f)
			this.ObjectBody.applyLinearImpulse(-25f * moveSpeed, 0, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);

		if (Przemieszczenie.x < -25f)
			this.ObjectBody.applyLinearImpulse(25f * moveSpeed, 0, this.ObjectBody.getPosition().x,
					this.ObjectBody.getPosition().y, true);
	}

	public boolean isMove() {
		return isMove;
	}

	public void setMove(boolean isMove) {
		this.isMove = isMove;
	}

	public void createBomb(Bomb bomba) {

<<<<<<< HEAD
		overpalsBomb = bomba;
		
		//int i = 8;
		if (kierunekRuchu == KierunekRuchu.Gora)
			bomba.setPos(this.ObjectBody.getPosition().x, this.ObjectBody.getPosition().y);
		if (kierunekRuchu == KierunekRuchu.Dol)
			bomba.setPos(this.ObjectBody.getPosition().x, this.ObjectBody.getPosition().y);
		if (kierunekRuchu == KierunekRuchu.Lewo)
			bomba.setPos(this.ObjectBody.getPosition().x, this.ObjectBody.getPosition().y);
		if (kierunekRuchu == KierunekRuchu.Prawo)
			bomba.setPos(this.ObjectBody.getPosition().x, this.ObjectBody.getPosition().y);

=======
		if (bomba.isCreate != true) {
			// int i = 8;
			isPlayerOverpalsBomb = true;
			overpalsBomb = bomba;
			int x = (int) (this.ObjectBody.getPosition().x) / Settings.Texture_size;
			int y = (int) (this.ObjectBody.getPosition().y) / Settings.Texture_size;
			if (kierunekRuchu == KierunekRuchu.Gora)
				bomba.setPos((x * Settings.Texture_size) + 8, y * Settings.Texture_size + 8);
			if (kierunekRuchu == KierunekRuchu.Dol)
				bomba.setPos((x * Settings.Texture_size) + 8, y * Settings.Texture_size + 8);
			if (kierunekRuchu == KierunekRuchu.Lewo)
				bomba.setPos((x * Settings.Texture_size) + 8, y * Settings.Texture_size + 8);
			if (kierunekRuchu == KierunekRuchu.Prawo)
				bomba.setPos((x * Settings.Texture_size) + 8, y * Settings.Texture_size + 8);

		}
>>>>>>> Box
	}

	public void reload() {
		this.ObjectBody.setTransform(20, 20, 0);

	}

}
