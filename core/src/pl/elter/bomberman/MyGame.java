package pl.elter.bomberman;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import pl.elter.bomberman.GeneralSettings.Settings;
import pl.elter.bomberman.Screen.GameScreen;

public class MyGame extends Game {
	
	@Override
	public void create () {
		this.setScreen(new GameScreen());
		Settings.texture =  new Texture("bomb_party_v4.png");
		
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
	}
}
