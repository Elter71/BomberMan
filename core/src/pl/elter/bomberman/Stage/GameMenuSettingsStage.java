package pl.elter.bomberman.Stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import pl.elter.bomberman.GeneralSettings.Settings;
import pl.elter.bomberman.Screen.GameScreen;

public class GameMenuSettingsStage extends Stage {

	private Table table;

	private boolean keychange[];

	private Label upText;
	private Label downText;
	private Label rightText;
	private Label leftText;
	private Label bombText;

	private Table cloneTable;
	private Window mainwindow;

	private boolean isKeysSettigsPress;

	private final int StartPlayerKeys[] = { Keys.W, Keys.S, Keys.A, Keys.D, Keys.SPACE };

	public GameMenuSettingsStage(final Stage backStage) {

		isKeysSettigsPress = false;

		table = new Table();
		table.setFillParent(true);
		Skin skin = Settings.GuiSkin;
		table.setFillParent(true);
		keychange = new boolean[5];

		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = skin.getFont("default");
		labelStyle.fontColor = skin.getColor("default");
		labelStyle.background = skin.getDrawable("button_01");

		skin.add("default", labelStyle, LabelStyle.class);

		TextButton ExitTable = new TextButton("Back to menu", skin.get("default", TextButtonStyle.class));

		WindowStyle Wstyle = new WindowStyle();
		Wstyle.titleFont = skin.getFont("default");
		Wstyle.titleFontColor = skin.getColor("default");
		Wstyle.background = skin.getDrawable("window_01");

		mainwindow = new Window("Settings", Wstyle);

		TextButton KeySetting = new TextButton("KeySetting", skin.get("default", TextButtonStyle.class));

		TextButton Exitwindow = new TextButton("Back", skin.get("default", TextButtonStyle.class));
		TextButton Reset = new TextButton("Reset", skin.get("default", TextButtonStyle.class));

		Label upOpis = new Label("Player Up", skin.get("default", LabelStyle.class));
		upText = new Label("W", skin.get("default", LabelStyle.class));
		TextButton upPress = new TextButton("Press to chage", skin.get("default", TextButtonStyle.class));

		Label downOpis = new Label("Player Down", skin.get("default", LabelStyle.class));
		downText = new Label("W", skin.get("default", LabelStyle.class));
		TextButton downPress = new TextButton("Press to chage", skin.get("default", TextButtonStyle.class));

		Label leftOpis = new Label("Player Left", skin.get("default", LabelStyle.class));
		leftText = new Label("W", skin.get("default", LabelStyle.class));
		TextButton leftPress = new TextButton("Press to chage", skin.get("default", TextButtonStyle.class));

		Label rightOpis = new Label("Player Right", skin.get("default", LabelStyle.class));
		rightText = new Label("W", skin.get("default", LabelStyle.class));
		TextButton rightPress = new TextButton("Press to chage", skin.get("default", TextButtonStyle.class));

		Label bombOpis = new Label("Bomb Spawn", skin.get("default", LabelStyle.class));
		bombText = new Label("W", skin.get("default", LabelStyle.class));
		TextButton bombPress = new TextButton("Press to chage", skin.get("default", TextButtonStyle.class));

		// table.add(label);
		// table.add(mainwindow);
		// keySetting windows
		mainwindow.setSize(300, 350);
		mainwindow.setPosition(200, 100);
		mainwindow.getTitleLabel().sizeBy(10);
		mainwindow.getTitleLabel().setFillParent(true);

		mainwindow.row().expandX().left();
		mainwindow.add(upOpis);
		mainwindow.add(upText);
		mainwindow.add(upPress);

		mainwindow.row().expandX().left();
		mainwindow.add(downOpis);
		mainwindow.add(downText);
		mainwindow.add(downPress);

		mainwindow.row().expandX().left();
		mainwindow.add(leftOpis);
		mainwindow.add(leftText);
		mainwindow.add(leftPress);

		mainwindow.row().expandX().left();
		mainwindow.add(rightOpis);
		mainwindow.add(rightText);
		mainwindow.add(rightPress);

		mainwindow.row().expandX().left();
		mainwindow.add(bombOpis);
		mainwindow.add(bombText);
		mainwindow.add(bombPress);

		mainwindow.row().expand(false, false).center().colspan(2);
		mainwindow.add(Exitwindow);
		mainwindow.add(Reset);

		// table
		table.add(KeySetting);
		table.row();
		table.add(ExitTable);

		// Lisner

		Reset.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				for (int i = 0; i < StartPlayerKeys.length; i++)
					Settings.Player1_key_map[i] = StartPlayerKeys[i];

				System.out.println("Start: " + StartPlayerKeys[0]);
				System.out.println("Player_1: " + Settings.Player1_key_map[0]);
				super.clicked(event, x, y);

			}

		});

		KeySetting.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				cloneTable = table;
				table.remove();
				isKeysSettigsPress = true;
				super.clicked(event, x, y);

			}

		});

		upPress.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				keychange[0] = true;
			}

		});

		downPress.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				keychange[1] = true;
			}

		});

		leftPress.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				keychange[2] = true;
			}

		});
		rightPress.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				keychange[3] = true;
			}

		});
		bombPress.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				keychange[4] = true;
			}

		});

		ExitTable.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameScreen.activeStage = backStage;
				super.clicked(event, x, y);
			}

		});
<<<<<<< HEAD
		//testestsetests
		ExitTable = Exitwindow;
		
		
=======
>>>>>>> Box

		Exitwindow.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				table = cloneTable;
				isKeysSettigsPress = false;
				cloneTable = mainwindow;
				mainwindow.remove();

				super.clicked(event, x, y);
			}

		});

	}

	@Override
	public boolean keyDown(int keyCode) {
		for (int i = 0; i < keychange.length; i++) {
			if (keychange[i]) {

				Settings.Player1_key_map[i] = keyCode;

				keychange[i] = false;
			}
		}
		return super.keyDown(keyCode);
	}

	@Override
	public void draw() {

		super.draw();
	}

	@Override
	public void act(float delta) {
		upText.setText(Keys.toString(Settings.Player1_key_map[0]));
		downText.setText(Keys.toString(Settings.Player1_key_map[1]));
		leftText.setText(Keys.toString(Settings.Player1_key_map[2]));
		rightText.setText(Keys.toString(Settings.Player1_key_map[3]));
		bombText.setText(Keys.toString(Settings.Player1_key_map[4]));

		if (isKeysSettigsPress)
			this.addActor(mainwindow);
		else {
			this.addActor(table);
		}

		super.act(delta);
	}

}
