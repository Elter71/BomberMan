package pl.elter.bomberman.Stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import pl.elter.bomberman.GeneralSettings.Settings;
import pl.elter.bomberman.Screen.GameScreen;

public class GameMenuStage extends Stage {

	private TextureAtlas Gui_texture;
	private BitmapFont font;
	private Skin skin;

	private Table table;
	
	public boolean isStartClick;
	
	private GameMenuSettingsStage settingsStage;

	public GameMenuStage() {
		Gui_texture = new TextureAtlas("Gui/ui-green.atlas");
		font = new BitmapFont();
		table = new Table();
		table.setFillParent(true);
		isStartClick = false;

		skin = new Skin(Gui_texture);

		skin.add("default", font);

		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.font = skin.getFont("default");
		textButtonStyle.fontColor = new Color(1, 1, 1, 1f);
		textButtonStyle.downFontColor = new Color(0, 0, 0, 0.7f);
		textButtonStyle.up = skin.getDrawable("button_01");
		textButtonStyle.down = skin.getDrawable("button_03");
		
		skin.add("default", textButtonStyle.fontColor,Color.class);

		skin.add("default", textButtonStyle, TextButtonStyle.class);

		
		
// Setting Skin set		
		Settings.GuiSkin = skin;
		
		settingsStage = new GameMenuSettingsStage(this);
		
		TextButton Start = new TextButton("Start", skin.get("default", TextButtonStyle.class));
		TextButton options = new TextButton("Options", skin.get("default", TextButtonStyle.class));
		TextButton Exit = new TextButton("Exit", skin.get("default", TextButtonStyle.class));

		table.add(Start);
		table.row();
		table.add(options);
		table.row();
		table.add(Exit);
		table.row();
		
		Start.addListener(new ClickListener(){

			@Override
			public void clicked(InputEvent event, float x, float y) {
				isStartClick = true;
				super.clicked(event, x, y);
			}
			
		});
		
		options.addListener(new ClickListener(){

			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameScreen.activeStage = settingsStage;
				super.clicked(event, x, y);
			}
			
		});
		
		
		
		
		Exit.addListener(new ClickListener(){

			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
				super.clicked(event, x, y);
			}
			
		});
		
		this.addActor(table);

	}

	@Override
	public void act(float delta) {
		// TODO Auto-generated method stub
		super.act(delta);
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		super.draw();
	}

}
