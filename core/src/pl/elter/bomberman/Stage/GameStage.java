package pl.elter.bomberman.Stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;

import pl.elter.bomberman.GameObject.Bomb;
import pl.elter.bomberman.GameObject.KierunekRuchu;
import pl.elter.bomberman.GameObject.Map;
import pl.elter.bomberman.GameObject.Player;
import pl.elter.bomberman.GeneralSettings.Settings;
import pl.elter.bomberman.WorldListener.BombContactListener;

public class GameStage extends Stage {

	private SpriteBatch batch;
	private OrthographicCamera mainCamera;

	private World GameWorld;

	private Box2DDebugRenderer b2ddr;

	private Map mainMap;

	private Player mainPlayer;

	private Bomb myBomba;
<<<<<<< refs/remotes/origin/master

	public boolean isGamePlay;
=======
>>>>>>> Revert "Merged Bomb into master"

	private int Player_Keys[];

	public GameStage() {

		this.batch = new SpriteBatch();
		b2ddr = new Box2DDebugRenderer();

		this.mainCamera = new OrthographicCamera();
		mainCamera.setToOrtho(false, Settings.Viewport_Width, Settings.Viewport_Height);

		GameWorld = new World(new Vector2(0, 0), true);

		mainMap = new Map("Bomber mapa", GameWorld);

		mainPlayer = new Player(new Texture("bomb_party_v4.png"), "Bomber", GameWorld);

		myBomba = new Bomb(new Texture("bomb_party_v4.png"), "Bomba", GameWorld, -500, -100);
<<<<<<< refs/remotes/origin/master
		myBomba.setWallMap(mainMap.Map);
<<<<<<< HEAD
		GameWorld.setContactListener(new BombContactListener(mainPlayer, myBomba, this));
=======
		GameWorld.setContactListener(new BombContactListener(mainPlayer,myBomba));
>>>>>>> Revert "Merged Bomb into master"
=======
		GameWorld.setContactListener(new BombContactListener(mainPlayer, myBomba,mainMap.boxlist, this));
>>>>>>> Box
	}

	@Override
	public void draw() {
		super.draw();

		mainMap.Draw(batch, mainCamera);

		batch.begin();
		mainPlayer.Draw(batch);

		if (myBomba != null)
			myBomba.Draw(batch);

		batch.end();

		//b2ddr.render(GameWorld, mainCamera.combined);
	}

	@Override
	public void act(float delta) {
		// TODO Auto-generated method stub
		super.act(delta);

		mainCamera.update();
		batch.setProjectionMatrix(mainCamera.combined);
		GameWorld.step(delta, 1, 1);
		mainPlayer.act(delta);
		myBomba.act(delta);
		keyMoveType();

		// sprawdzenie czy gracz jest przy bombie

		// if (myBomba.overlaps(mainPlayer) && myBomba.isCreate) {
		// System.out.println("overrlaps");

		// }
		// myBomba.isOverrlaps(mainPlayer);PlayerKeysChar

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
	}

	@Override
	public boolean keyUp(int keyCode) {
		if (keyCode == Player_Keys[0] || keyCode == Player_Keys[1] || keyCode == Player_Keys[2]
				|| keyCode == Player_Keys[3])
			mainPlayer.setMove(false);
<<<<<<< refs/remotes/origin/master

=======
>>>>>>> Revert "Merged Bomb into master"
		return super.keyUp(keyCode);
	}

	private boolean keyMoveType() {
		
		
		if (Gdx.input.isKeyPressed(Player_Keys[0]) && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Gora);
		if (Gdx.input.isKeyPressed(Player_Keys[1]) && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Dol);
		if (Gdx.input.isKeyPressed(Player_Keys[2]) && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Lewo);
		if (Gdx.input.isKeyPressed(Player_Keys[3]) && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Prawo);
		
		return true;
	}

	/*@Override
	public boolean keyTyped(char character) {
		// TODO zrob zeby mozna bylo isc tylko w jedna strone czyli dziala jeden
		// if w
		// ktorym kierunku sie porusza postac

		System.out.println(character);

		if (character == PlayerKeysChar[0] && mainPlayer.isMove() != true) // sposob
																			// na
																			// zapisanie
			mainPlayer.movePlayer(KierunekRuchu.Gora);
		if (character == PlayerKeysChar[1] && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Dol);
		if (character == PlayerKeysChar[2] && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Lewo);
		if (character == PlayerKeysChar[3] && mainPlayer.isMove() != true)
			mainPlayer.movePlayer(KierunekRuchu.Prawo);

		return super.keyTyped(character);

	}*/

	@Override
	public boolean keyDown(int keyCode) {
		// TODO Auto-generated method stub

		if (keyCode == Player_Keys[4]) {
			mainPlayer.createBomb(myBomba);

		}
<<<<<<< refs/remotes/origin/master

		if (keyCode == Keys.CONTROL_LEFT) {
			mainPlayer.sayMyPos();
			System.out.println("Pos : 16: "+mainPlayer.getBody().getPosition().x/16+"|"+mainPlayer.getBody().getPosition().y/16);
		}

		if (keyCode == Keys.ESCAPE)
			isGamePlay = false;

=======
		if(keyCode == Keys.SHIFT_LEFT)
			myBomba.Fixturechange((short)6);
		
>>>>>>> Revert "Merged Bomb into master"
		return super.keyDown(keyCode);
	}

	public void reload() {
		mainPlayer.reload();
		this.isGamePlay = true;
		mainMap.reload();

		myBomba.reload();
		Player_Keys = Settings.Player1_key_map;

	}
}
