package pl.elter.bomberman.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pl.elter.bomberman.MyGame;
import pl.elter.bomberman.GeneralSettings.Settings;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width= Settings.Game_Width;
		config.height= Settings.Game_Height;
		config.title = Settings.Title;
		new LwjglApplication(new MyGame(), config);
	}
}
